# whatifshu

a charting exploration of (un)known what-ifs  
also known as repositories  

Terminal:
```
cd ~/org/roam
git clone https://github.com/tekakutli/whatifshu
```
and add this to your roam .gitignore so it doesn't get mixed-in with yours  
```
roam/whatifshu/
```
# WHAT?
have you gone on an exploration to find a repository to fulfill your needs?  
if so, consider participating in this journal  

an index of the repositories that you wanted to find and those that just you happen to discover  
add metadata about how they connect to one another, the advantages of one above another, etc  
or add any extra insight that you learn about them that others should learn about of

# WHAT ELSE IS THIS
An attempt at an experiment, where people'd share bits of their org-roams, known as [org-book](https://lemy.lol/c/orgbooks)(more on this lemmy-fediverse community)
# ANY REPOSITORY?
maybe some of them would rather be on other orgbook of themselves?  
like, I'm keeping neural-network related ones in another orgbook: neuralnomicon

# HOW TO USE
however you develop your investigation  
but I propose an extra tag(org tags) system on top to help drive a seeker attention  
like, you could use the tag :coolest: on the appropriate headings  
## TAGS USED SO FAR
:coolest:
# TEMPLATE
To create nodes for this directory I have a template.
config.el
```
(setq org-roam-capture-templates
      '(
        ("n" "default" plain
         "%?"
         :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
         :unnarrowed t)
        ("w" "whatifshu" plain
         "%?"
         :if-new (file+head "whatifshu/nodes/%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+filetags: :whatifshu:\n")
         :unnarrowed t)
        )
)
```
